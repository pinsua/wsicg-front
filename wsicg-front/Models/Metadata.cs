﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WSICG_Front.Models
{
    public class UsuarioMetadata
    {
        [Display(Name = "Identificador de Usuario")]
        public int Id;

        [StringLength(50)]
        [Display(Name = "Nombre de Usuario")]
        public string Usuario;

        [DataType("Password")]
        [StringLength(48, MinimumLength = 6)]
        [Display(Name = "Contraseña")]
        public string Password;

        [Display(Name = "Último Acceso")]
        public Nullable<System.DateTime> UltimoAcceso;

        [Display(Name = "Usuario Activo")]
        public Nullable<Boolean> Activo;
    }

    public class PermisoMetadata
    {
        [Display(Name = "Identificador del Permiso")]
        public int Id;

        [Display(Name = "Identificador del Usuario")]
        public int IdUsuario { get; set; }

        [Display(Name = "Entry Point")]
        public string Url { get; set; }

        [Display(Name = "Permitir Acceso?")]
        public bool Permitir { get; set; }        
    }
}