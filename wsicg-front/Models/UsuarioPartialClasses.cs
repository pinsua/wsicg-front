﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WSICG_Front.CustomAuthentication;

namespace WSICG_Front.Models
{
    [MetadataType(typeof(UsuarioMetadata))]
    public partial class WS_USUARIOS
    {
        [DataType("Password")]
        [Display(Name = "Confirmar Cotraseña")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string Email { get; internal set; }        
    }
}