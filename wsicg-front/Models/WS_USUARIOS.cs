//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSICG_Front.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WS_USUARIOS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WS_USUARIOS()
        {
            this.Roles = "USER";
            this.WS_PERMISOS = new HashSet<WS_PERMISOS>();
        }
    
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> UltimoAcceso { get; set; }
        public bool Activo { get; set; }
        public bool IsAdmin { get; set; }
        public string Roles { get; set; }
        public string Salt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WS_PERMISOS> WS_PERMISOS { get; set; }
    }
}
