﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WSICG_Front.Models
{
    public class LoginView
    {
        [Required]
        [Display(Name = "Nombre de Usuario")]
        public string Usuario { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Es Administrador?")]
        public bool IsAdmin { get; set; }
        [Display(Name = "Role")]
        public string Role { get; set; }
    }

    public class CustomSerializeModel
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public bool IsAdmin { get; set; }
        public string Role { get; set; }

    }

}