﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WSICG_Front
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "permisos",
                url: "permisos/{action}/{id}",
                defaults: new
                {
                    controller = "WS_Permisos",
                    action = "Index",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "usuarios",
                url: "usuarios/{action}/{id}",
                defaults: new {
                    controller = "WS_USUARIOS",
                    action = "Index",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Root",
                url: "",
                defaults: new { controller = "Account", action = "Login" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "/",
                url: "usuarios/{action}/{id}",
                defaults: new
                {
                    controller = "WS_USUARIOS",
                    action = "Index",
                    id = UrlParameter.Optional
        }
            );
        }
    }
}
