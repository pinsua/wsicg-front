﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Mvc;
using WSICG_Front.CustomAuthentication;
using WSICG_Front.Models;
using WSICG_Front.Models.MethodInfo;

namespace WSICG_Front.Controllers
{
    public class WS_PERMISOSController : Controller
    {
        private DBEntities db = new DBEntities();

        private IEnumerable GetEntrysPoint() {
            IEnumerable<ApiDescription> members = null;
            
            using (var client = new HttpClient())
            {

                //client.BaseAddress = new Uri("http://localhost:64144/api/");
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["URLAPI"].ToString());
                var responseTask = client.GetAsync("apimethod");
                responseTask.Wait();
                var result = responseTask.Result;
                
                if (result.IsSuccessStatusCode)
                {
                    
                    var readTask = result.Content.ReadAsAsync<IList<Models.MethodInfo.ApiDescription>>();
                    readTask.Wait();
                    members = readTask.Result;
                }
                else
                {
                    members = Enumerable.Empty<Models.MethodInfo.ApiDescription>();
                    ModelState.AddModelError(string.Empty, String.Format("No se ha podido contactar con el servidor. URL: {0}",client.BaseAddress.AbsoluteUri));
                }
            }
            return members;
            
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Index()
        {
            var wS_PERMISOS = db.WS_PERMISOS.Include(w => w.WS_USUARIOS);
            return View(wS_PERMISOS.ToList());
        }

        [CustomAuthorize(Roles = "ADMIN")]        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_PERMISOS wS_PERMISOS = db.WS_PERMISOS.Find(id);
            if (wS_PERMISOS == null)
            {
                return HttpNotFound();
            }
            
            
            return View(wS_PERMISOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Create()
        {
            /*
            ApiMethodController metodos = new ApiMethodController();
            */
            ViewBag.EntrysPoint = new SelectList(GetEntrysPoint(), "RelativePath", "RelativePath");
            
            ViewBag.idUsuario = new SelectList(db.WS_USUARIOS.Where<WS_USUARIOS>(x => (x.Activo) && (!x.Roles.Contains("ADMIN"))).ToList(), "Id", "Usuario");
            return View();
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,idUsuario,url,permitir")] WS_PERMISOS wS_PERMISOS)
        {
            var existe = from x in db.WS_PERMISOS
                         where wS_PERMISOS.IdUsuario.Equals(x.IdUsuario) && wS_PERMISOS.Url.Equals(x.Url)
                         select x.Id;


            if (existe.Count()==1) {
                ModelState.AddModelError(string.Empty, "El permiso ya existe.");
                ViewBag.EntrysPoint = new SelectList(GetEntrysPoint(), "RelativePath", "RelativePath");

                ViewBag.idUsuario = new SelectList(db.WS_USUARIOS.Where<WS_USUARIOS>(x => (x.Activo) && (!x.Roles.Contains("ADMIN"))).ToList(), "Id", "Usuario");
                return View();
            }

            if (ModelState.IsValid)
            {
                db.WS_PERMISOS.Add(wS_PERMISOS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.EntrysPoint = null; // metodos.GetMethods(); // new SelectList(metodos.GetMethods(), "RelativePath", "RelativePath", wS_PERMISOS.Url);            
            //ViewBag.idUsuario = new SelectList(db.WS_USUARIOS, "Id", "Usuario", wS_PERMISOS.IdUsuario);
            return View(wS_PERMISOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_PERMISOS wS_PERMISOS = db.WS_PERMISOS.Find(id);
            if (wS_PERMISOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUsuario = new SelectList(db.WS_USUARIOS, "Id", "Usuario", wS_PERMISOS.IdUsuario);
            return View(wS_PERMISOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,idUsuario,url,permitir")] WS_PERMISOS wS_PERMISOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wS_PERMISOS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idUsuario = new SelectList(db.WS_USUARIOS, "Id", "Usuario", wS_PERMISOS.IdUsuario);
            return View(wS_PERMISOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_PERMISOS wS_PERMISOS = db.WS_PERMISOS.Find(id);
            if (wS_PERMISOS == null)
            {
                return HttpNotFound();
            }
            return View(wS_PERMISOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WS_PERMISOS wS_PERMISOS = db.WS_PERMISOS.Find(id);
            db.WS_PERMISOS.Remove(wS_PERMISOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
