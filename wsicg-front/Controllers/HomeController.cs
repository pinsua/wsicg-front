﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WSICG_Front.Models;

namespace WSICG_Front.Controllers
{
    public class HomeController : Controller
    {
        private DBEntities db = new DBEntities();

        public ActionResult Index()
        {
            return View();
        }           

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}