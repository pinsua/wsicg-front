﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WSICG_Front.CustomAuthentication;
using WSICG_Front.Models;

namespace WSICG_Front.Controllers
{
    public class AccountController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: Account
        [AllowAnonymous]
        public ActionResult Index()
        {
            bool isAdmin = User.IsInRole("ADMIN");

            if (isAdmin)
            {
                ViewBag.Rol = "ADMINISTRADOR";
            }
            else {
                ViewBag.Rol = "NO ADMINISTRADOR";
            }

            ViewBag.Usuario = User.Identity.Name;            
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginView loginView, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {


                var salt = from c in db.WS_USUARIOS
                           where c.Usuario.Equals(loginView.Usuario)
                           select c.Salt;
                if (salt.Count()==0 )
                {
                    ModelState.AddModelError(string.Empty, "Usuario no valido");
                    return RedirectToAction("Index");
                }

                string pass = Helper.EncodePassword(loginView.Password, salt.First<string>());
                //if (Membership.ValidateUser(loginView.Usuario, loginView.Password))
                if (Membership.ValidateUser(loginView.Usuario, pass))
                {
                    var user = (CustomMembershipUser)Membership.GetUser(loginView.Usuario, false);
                    if (user != null)
                    {
                        CustomSerializeModel userModel = new Models.CustomSerializeModel()
                        {
                            Id = user.Id,
                            Usuario = user.Usuario,
                            IsAdmin = user.IsAdmin,
                            Role = user.Roles.First<Role>().RoleName
                        };

                        string userData = JsonConvert.SerializeObject(userModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                            1, loginView.Usuario, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
                            );

                        string enTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie("Cookie1", enTicket);
                        Response.Cookies.Add(faCookie);
                    }

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else {
                    ModelState.AddModelError(string.Empty, "No se ha podido validar sus credenciales");
                }
            }
            ModelState.AddModelError("", "Something Wrong : Username or Password invalid ^_^ ");
            return View(loginView);
        }


        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("Cookie1", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", null);
        }

    }
}