﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WSICG_Front.CustomAuthentication;
using WSICG_Front.Models;

namespace WSICG_Front.Controllers
{
    public class WS_USUARIOSController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: WS_USUARIOS
        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Index()
        {
            return View(db.WS_USUARIOS.Where<WS_USUARIOS>(x =>  (!x.Roles.Contains("ADMIN"))).ToList());
        }

        // GET: WS_USUARIOS/Details/5
        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_USUARIOS wS_USUARIOS = db.WS_USUARIOS.Find(id);
            if (wS_USUARIOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.permisos = wS_USUARIOS.WS_PERMISOS.ToList<WS_PERMISOS>();
            return View(wS_USUARIOS);
        }

        
        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Create()
        {
            return View();
        }

        //[CustomAuthorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Usuario,Password,ConfirmPassword,UltimoAcceso,Activo,IsAdmin")] WS_USUARIOS wS_USUARIOS)
        {
            string salt = Helper.GeneratePassword(10);
            if (ModelState.IsValid)
            {
                try
                {
                    wS_USUARIOS.Salt = salt;
                    wS_USUARIOS.Password = Helper.EncodePassword(wS_USUARIOS.Password, salt);
                    wS_USUARIOS.Roles = "USER";
                    wS_USUARIOS.ConfirmPassword = wS_USUARIOS.Password;
                    db.WS_USUARIOS.Add(wS_USUARIOS);

                    WS_PERMISOS wS_PERMISOS = new WS_PERMISOS();
                    wS_PERMISOS.IdUsuario = wS_USUARIOS.Id;
                    wS_PERMISOS.Permitir = true;
                    wS_PERMISOS.Url = "GET /api/testauth";
                    db.WS_PERMISOS.Add(wS_PERMISOS);

                    db.SaveChanges();
                }
                catch (Exception exc)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }

            return View(wS_USUARIOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_USUARIOS wS_USUARIOS = db.WS_USUARIOS.Find(id);
            if (wS_USUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(wS_USUARIOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Roles,Id,Usuario,Password,ConfirmPassword,UltimoAcceso,Activo,IsAdmin")] WS_USUARIOS wS_USUARIOS)
        {
            string salt = Helper.GeneratePassword(10);
            if (ModelState.IsValid)
            {
                db.Entry(wS_USUARIOS).State = EntityState.Modified;
                if (string.IsNullOrEmpty(wS_USUARIOS.Password))
                {
                    db.Entry(wS_USUARIOS).Property(x => x.Password).IsModified = false;
                }
                else {
                    wS_USUARIOS.Salt = salt;
                    wS_USUARIOS.Password = Helper.EncodePassword(wS_USUARIOS.Password, salt);
                    wS_USUARIOS.ConfirmPassword = wS_USUARIOS.Password;                    
                }
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(wS_USUARIOS);
        }


        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult ChangePassword()
        {            
            WS_USUARIOS wS_USUARIOS = db.WS_USUARIOS.Find(1);
            if (wS_USUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(wS_USUARIOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword([Bind(Include = "Roles,Id,Usuario,Password,ConfirmPassword,UltimoAcceso,Activo,IsAdmin")] WS_USUARIOS wS_USUARIOS)
        {
            string salt = Helper.GeneratePassword(10);
            if (ModelState.IsValid)
            {
                db.Entry(wS_USUARIOS).State = EntityState.Modified;
                if (string.IsNullOrEmpty(wS_USUARIOS.Password))
                {
                    db.Entry(wS_USUARIOS).Property(x => x.Password).IsModified = false;
                }
                else
                {
                    wS_USUARIOS.Salt = salt;
                    wS_USUARIOS.Password = Helper.EncodePassword(wS_USUARIOS.Password, salt);
                    wS_USUARIOS.ConfirmPassword = wS_USUARIOS.Password;
                }
                wS_USUARIOS.Activo = true;
                wS_USUARIOS.IsAdmin = true;
                wS_USUARIOS.Roles = "ADMIN";
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(wS_USUARIOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WS_USUARIOS wS_USUARIOS = db.WS_USUARIOS.Find(id);
            if (wS_USUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(wS_USUARIOS);
        }

        [CustomAuthorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WS_USUARIOS wS_USUARIOS = db.WS_USUARIOS.Find(id);
            db.WS_USUARIOS.Remove(wS_USUARIOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
