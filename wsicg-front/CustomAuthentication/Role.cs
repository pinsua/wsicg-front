﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSICG_Front.CustomAuthentication
{
    public class Role
    {        
        public string RoleName { get; set; }

        public Role(string s) {
            RoleName = s;
        }
    }
}