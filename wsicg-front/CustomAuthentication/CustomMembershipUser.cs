﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WSICG_Front.Models;

namespace WSICG_Front.CustomAuthentication
{
    public class CustomMembershipUser : MembershipUser
    {
        #region User Properties

        public int Id { get; set; }
        public string Usuario { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<Role> Roles { get; set; }

        #endregion

        public CustomMembershipUser(WS_USUARIOS user):base("CustomMembership", user.Usuario, user.Id, user.Email, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            Id = user.Id;
            Usuario = user.Usuario;
            IsAdmin = user.IsAdmin;
            Roles = user.Roles.Split(new string[] { "\r\n" }, StringSplitOptions.None)
              .Select(value => new Role(value))  // "convert" strings to `role` 
              .ToList();
            
        }
    }
}