﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSICG_Front.CustomAuthentication
{
    public class User
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public Guid ActivationCode { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

    }
}