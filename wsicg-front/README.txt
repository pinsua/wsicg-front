﻿WSICG-Front
===========

CONFIGURACION
-------------
En el archivo web.config se deberá establecer en la connectionstring llamada DBEntities los
valores correspondientes a la instancia del Servidor SQLServer, credenciales y nombre de la BD.

Además se deberá establecer la URL donde estará corriendo WSICG-API. 

QUERYS
------

-- TABLA USUARIOS
 

CREATE TABLE PATAGONIA.dbo.WS_USUARIOS (
  Id int IDENTITY,
  Usuario varchar(50) NULL,
  Password varchar(50) NULL,
  UltimoAcceso datetime NULL,
  Activo bit NOT NULL CONSTRAINT DF__WS_USUARI__Activ__267ABA7A DEFAULT (0),
  IsAdmin bit NOT NULL CONSTRAINT DF__WS_USUARI__IsAdm__276EDEB3 DEFAULT (0),
  Roles varchar(50) NOT NULL,
  Salt varchar(10) NULL,
  CONSTRAINT PK__WS_USUAR__3214EC0763C9E26A PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO


-- TABLA PERMISOS

CREATE TABLE PATAGONIA.dbo.WS_PERMISOS (
  id int IDENTITY,
  idUsuario int NOT NULL,
  url varchar(100) NOT NULL,
  permitir bit NOT NULL,
  CONSTRAINT PK_WS_PERMISOS_ID PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO

ALTER TABLE PATAGONIA.dbo.WS_PERMISOS
  ADD CONSTRAINT FK_WS_PERMISOS_idUsuario FOREIGN KEY (idUsuario) REFERENCES dbo.WS_USUARIOS (Id)
GO

HowTo
-----
Acceso: http://[host]/Home
Documentación: http://[host]/swagger/ui/index#/

USUARIO ADMIN
=============
el usuario con ID 1 es el usuario admin principal. 
En caso de necesitar resetear la contraseña o querer modificar
la dirección de correo electrónico, ejecutar este script para setear el password
como: "_P455w0rd".
Luego desde el panel de control es posible modificarlo una vez iniciada la sesión.

UPDATE dbo.WS_USUARIOS SET
	Password = 'D1-22-4F-0B-30-69-E3-55-A5-1C-45-3C-2B-AF-63-14', -- _P455w0rd
	Salt = '4bjLVO7yib',
	IsAdmin = 1,
	Roles = 'ADMIN',
  Usuario = 'admin@icg.com'
WHERE Id = 1